//
//  JKCenterScrollView.h
//  ImageTransition
//
//  Created by Joris Kluivers on 1/14/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSInteger JKCenterViewTag = 99;

@interface JKCenterScrollView : UIScrollView

@end
