//
//  JKImageViewController.m
//  ImageTransition
//
//  Created by Joris Kluivers on 1/12/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "JKImageViewController.h"
#import "JKCenterScrollView.h"

@interface JKImageViewController ()

@end

@implementation JKImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.imageView.tag = JKCenterViewTag;
}

- (void) viewWillAppear:(BOOL)animated
{
	/*if (animated) {
		[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
	} else {
		[UIApplication sharedApplication].statusBarHidden = YES;
	}*/
	
	self.imageView.hidden = YES;
	self.imageView.image = self.image;
}

- (void) viewDidAppear:(BOOL)animated
{
	self.imageView.hidden = NO;
	
	CGFloat factor = fminf(CGRectGetWidth(self.scrollView.frame) / self.image.size.width, CGRectGetHeight(self.scrollView.frame) / self.image.size.height);
	
	CGRect position = CGRectZero;
	position.size = CGSizeMake(self.image.size.width*factor, self.image.size.height * factor);
	self.imageView.frame = position;
	self.imageView.center = CGPointMake(CGRectGetMidX(self.scrollView.frame), CGRectGetMidY(self.scrollView.frame));
	
	self.scrollView.contentSize = position.size;	
}

- (void) viewWillDisappear:(BOOL)animated
{
	//[UIApplication sharedApplication].statusBarHidden = NO;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	NSLog(@"%s", __func__);
}

#pragma mark - Scroll & Zoom

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.imageView;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (scrollView.zoomScale > 1.0) {
		return;
	}
	
	if (fabsf(scrollView.contentOffset.y) < 50.0f) {
		return;
	}
	
	NSLog(@"Dismiss!");
	
	[self performSegueWithIdentifier:@"ExitSegue" sender:self];
}

@end
